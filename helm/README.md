# SoundBuzz Helm Charts

Manage easily Soundbuzz wih this **Helm** charts.

## Usage

Create a namespace or use the flag `--create-namespace` into the **Helm** commande :

```bash
kubectl create ns namespace
```

At the racine of the project, install charts :

```bash
helm install release-name helm -n namespace
```

Configure the helm deployment by settings parameters :

```bash
helm install release-name helm -n namespace --set sb.tag=0.47.5
```

Install with a values file sourced :

```bash
helm install release-name helm -n namespace -f helm/values-prod.yaml
```

Once the charts installed, juste upgrade its :

```bash
helm upgrade release-name helm -n namespace -f helm/values-prod.yaml
```

Remove the **Helm** deployment simply by the following command :

```bash
helm uninstall release-name helm -n namespace
```

## Settings

Set the deployment :

| Parameter | Description | Default |
|:----------|:------------|:--------|
| nameOverride | Override the name of the deployment | Empty |
| namespaceOverride | Override the namespace of the deployment | Empty |

Set the pod :

| Parameter | Description | Default |
|:----------|:------------|:--------|
| sb.image | Specify the image to use | registry.gitlab.com/mln-nicolas-b/soundbuzz |
| sb.tag | Specify the image tag to use | latest |

Set the horizontal pod autoscaling :

| Parameter | Description | Default |
|:----------|:------------|:--------|
| sb.autoscaling.enabled | Enable pod autoscaling | false |
| sb.autoscaling.minReplicas | Set the minimum pod replicas | 1 |
| sb.autoscaling.maxReplicas | Set the maximum pod replicas | 3 |
| sb.autoscaling.targetCPUUtilizationPercentage | Specify the limit cpu percentage to scale | 20 |
| sb.autoscaling.targetMemoryUtilizationPercentage | Specify the limit memory percentage to scale | 50 |

Set the ingress :

| Parameter | Description | Default |
|:----------|:------------|:--------|
| sb.ingress.enabled | Enable the ingress | false |
| sb.ingress.domain | Specify the ingress domain | Empty |
| sb.ingress.annotations | Specify the ingress type to use | "kubernetes.io/ingress.class: nginx". Use the Nginx ingress controller |
| sb.ingress.tls.enabled | Enable TLS encryption | false |
| sb.ingress.tls.annotations | Specify the issuer to use | "cert-manager.io/cluster-issuer: letsencrypt-prod". Use Cert Manager for TLS encryption |

Set the application :

| Parameter | Description | Default |
|:----------|:------------|:--------|
| sb.env.ND_DATAFOLDER | Folder where your music library is stored | /datas. **Is required for the deployment** |
| sb.env.ND_MUSICFOLDER | Folder to store application data (DB, cache…) | /musics. **Is required for the deployment** |
| sb.env.ND_PORT | HTTP port Soundbuzz will use | 4533. **Is required for the deployment** |
| sb.env.ND_SPOTIFY_ID | Spotify Client ID. Required if you want Artist images | Empty |
| sb.env.ND_SPOTIFY_SECRET | Spotify Client Secret. Required if you want Artist images | Empty |
| sb.env.ND_LOGLEVEL | Log level. Useful for troubleshooting. Possible values: error, warn, info, debug, trace | debug |
| sb.env.ND_SCANSCHEDULE | Configure periodic scans using “cron” syntax. To disable it altogether, set it to "0" | @every 15m |
| sb.env.ND_ENABLELOGREDACTING | Whether or not sensitive information (like tokens and passwords) should be redacted (hidden) in the logs | true |
| sb.env.ND_ADDRESS | IP address the server will bind to | 0.0.0.0 |
| sb.env.ND_ENABLETRANSCODINGCONFIG | Enables transcoding configuration in the UI | false |
| sb.env.ND_TRANSCODINGCACHESIZE | Size of transcoding cache. Set to "0" to disable cache | 100MB |
| sb.env.ND_IMAGECACHESIZE | Size of image (art work) cache. Set to "0" to disable cache | 100MB |
| sb.env.ND_AUTOIMPORTPLAYLISTS | Enable/disable .m3u playlist auto-import | true |
| sb.env.ND_PLAYLISTSPATH | Where to search for and import playlists from. Can be a list of folders/globs (separated by : (or ; on Windows). Paths are relative to MusicFolder | .:**/** |
| sb.env.ND_BASEURL | Base URL (only the path part) to configure Navidrome behind a proxy (ex: /music) | Empty |
| sb.env.ND_UILOGINBACKGROUNDURL | Change background image used in the Login page | Empty |
| sb.env.ND_UIWELCOMEMESSAGE | Add a welcome message to the login screen | Empty |
| sb.env.ND_GATRACKINGID | Send basic info to your own Google Analytics account. Must be in the format UA-XXXXXXXX | Empty |
| sb.env.ND_DEFAULTTHEME | Sets the default theme used by the UI when logging in from a new browser. This value must match one of the options in the UI | Dark |
| sb.env.ND_ENABLECOVERANIMATION | Controls whether the player in the UI will animate the album cover (rotation) | true |
| sb.env.ND_IGNOREDARTICLES | List of ignored articles when sorting/indexing artists | The El La Los Las Le Les Os As O A |
| sb.env.ND_SEARCHFULLSTRING | Match query strings anywhere in searchable fields, not only in word boundaries. Useful for languages where words are not space separated | false |
| sb.env.ND_RECENTLYADDEDBYMODTIME | Uses music files’ modification time when sorting by “Recently Added”. Otherwise use import time | false |
| sb.env.ND_COVERARTPRIORITY | Configure the order to look for cover art images. Use special embedded value to get embedded images from the audio files | embedded, cover.*, folder.*, front.* |
| sb.env.ND_COVERJPEGQUALITY | Set JPEG quality percentage for resized cover art images | 75 |
| sb.env.ND_ENABLEDOWNLOADS | Enable the option in the UI to download music/albums/artists/playlists from the server | true |
| sb.env.ND_SESSIONTIMEOUT | How long Navidrome will wait before closing web ui idle sessions | 24h |
| sb.env.ND_AUTHREQUESTLIMIT | How many login requests can be processed from a single IP during the AuthWindowLength. Set to 0 to disable the limit rater | 5 |
| sb.env.ND_AUTHWINDOWLENGTH | Window Length for the authentication rate limit | 20s |
| sb.env.ND_SCANNER_EXTRACTOR | Select metadata extractor implementation. Options: taglib or ffmpeg | taglib |
| sb.env.ND_SCANNER_GENRESEPARATORS | List of separators to split genre tags | ;/, |
| sb.env.ND_LASTFM_ENABLED | Set this to false to completely disable Last.fm integration | true |
| sb.env.ND_LASTFM_APIKEY | Last.fm ApiKey Navidrome project’s shared ApiKey | Empty |
| sb.env.ND_LASTFM_SECRET | Last.fm Shared Secret Navidrome project’s shared Secret | Empty |
| sb.env.ND_LASTFM_LANGUAGE | Two letter-code for language to be used to retrieve biographies from Last.fm | fr |
| sb.env.ND_LISTENBRAINZ_ENABLED | Set this to false to completely disable ListenBrainz integration | true |
| sb.env.ND_ENABLEGRAVATAR | Use Gravatar images as the user profile image. Needs the user’s email to be filled | false |
| sb.env.ND_ENABLEEXTERNALSERVICES | Set this to false to completely disable ALL external integrations | true |
| sb.env.ND_ENABLEFAVOURITES | Enable toggling “Heart”/“Loved” for songs/albums/artists in the UI (maps to “Star”/“Starred” in Subsonic Clients) | true |
| sb.env.ND_ENABLESTARRATING | Enable 5-star ratings in the UI | true |
| sb.env.ND_ENABLEUSEREDITING | Enable regular users to edit their details and change their password | true |
| sb.env.ND_PASSWORDENCRYPTIONKEY | Passphrase used to encrypt passwords in the DB | wQciD94MPSRrNL75cSME |
| sb.env.ND_REVERSEPROXYUSERHEADER | HTTP header containing user name from authenticated proxy | Remote-User |
| sb.env.ND_REVERSEPROXYWHITELIST | Comma separated list of IP CIDRs which are allowed to use reverse proxy authentication, empty means “deny all” | Empty |
