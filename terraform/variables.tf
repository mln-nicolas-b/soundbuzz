variable "ovh_endpoint" {}
variable "ovh_application_key" {}
variable "ovh_application_secret" {}
variable "ovh_consumer_key" {}

variable "service_name" {}

variable "k8s_cluster_name" {}
variable "k8s_cluster_region" {}
variable "k8s_cluster_version" {}

variable "k8s_node_flavor" {}
variable "k8s_node_desired" {}
variable "k8s_node_max" {}
variable "k8s_node_min" {}
