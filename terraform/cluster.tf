resource "ovh_cloud_project_kube" "kubecluster" {
   service_name = "${var.service_name}"
   name         = "${var.k8s_cluster_name}"
   region       = "${var.k8s_cluster_region}"
   version      = "${var.k8s_cluster_version}"
}

resource "ovh_cloud_project_kube_nodepool" "nodepool" {
   service_name  = "${var.service_name}"
   kube_id       = ovh_cloud_project_kube.kubecluster.id
   name          = "${var.k8s_cluster_name}"
   flavor_name   = "${var.k8s_node_flavor}"
   desired_nodes = var.k8s_node_desired
   max_nodes     = var.k8s_node_max
   min_nodes     = var.k8s_node_min
}