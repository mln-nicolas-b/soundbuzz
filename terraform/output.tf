output "kubeconfig" {
  value = ovh_cloud_project_kube.kubecluster.kubeconfig
  sensitive = true
}
