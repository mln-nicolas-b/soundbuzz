#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from locust import HttpUser, task

class HelloWorldUser(HttpUser):
    @task
    def hello_world(self):
        self.client.get("/")
        self.client.get("/app/#/album/d6f2e67009217f02d8478bb0b35d0c03/show")
