# soundbuzz

This project is a real-life situation. SoundBuzz does not really exist and neither does the proposed product. It is part of an end of year project linked to my school curriculum.  
The deployment is based on Navidrome :
* Website: https://www.navidrome.org/
* Documentation: https://www.navidrome.org/docs/
* Github: https://github.com/navidrome/navidrome

## Manually push on registry

Login Docker on the registry :

```bash
docker login registry.gitlab.com -u User -p PersonnalAccessToken
```

Build Docker images :

```bash
IMAGES_TAG="0.47.5"; docker build -t registry.gitlab.com/mln-nicolas-b/soundbuzz:$IMAGES_TAG -t registry.gitlab.com/mln-nicolas-b/soundbuzz:latest soundbuzz
```

Push Docker images on the registry :

* The version tagged image :

```bash
docker push registry.gitlab.com/mln-nicolas-b/soundbuzz:$IMAGES_TAG
```

* The latest tagged image :

```bash
docker push registry.gitlab.com/mln-nicolas-b/soundbuzz:latest
```

## Deploy managed Kubernetes cluster

There are **terraform** manifests to deploy a managed Kubernetes cluster.
The cluster is managed by **OVH Cloud** in their public cloud.

Move to the folder *terraform* :

```bash
cd terrraform
```

Init the dependancies needed by **Terraform** to deploy the cluster :

```bash
terraform init
```

Plan the deployment :

> Do not forget to control the actions that will be taken.

```bash
terraform plan
```

Launch the deployment :

> Type yes for approve the deployment after checking it.

```bash
terraform apply
```

Get the kubeconfig from the administration interface :

![ovh_kubeconfig](docs/ovh_kubeconfig.png)

Get the kubeconfig from the terraform output :

```bash
terraform output kubeconfig > /home/$USER/.kube/soundbuzz.yaml
```

> It is possible the kuconfig generated isn't clean. Delete the first and last lines to fix it.

Configure `kubectl` :

```bash
export KUBECONFIG="/home/$USER/.kube/soundbuzz.yaml"
```

Make kubeconfig readable only by the owner :

```bash
chmod 0600 /home/$USER/.kube/soundbuzz.yaml
```

## Deploy on a Kubernetes cluster

Create a namespace:

```bash
kubectl create ns soundbuzz
```

Create persistent volumes :

```bash
kubectl apply -f kubernetes/soundbuzz-persistentvolumeclaim.yaml
```

Create environment :

```bash
kubectl apply -f kubernetes/soundbuzz-env-configmap.yaml
```

Deploy the application :

```bash
kubectl apply -f kubernetes/soundbuzz-deployment.yaml
```

Internal exposure of the deployment :

```bash
kubectl apply -f kubernetes/soundbuzz-service.yaml
```

### Use HTTPS protocole

Use **Cert Manager** for manage the TLS certificates :

Add the **Helm** repository of the **Cert Manager**  :

```bash
helm repo add jetstack https://charts.jetstack.io
```

Update **Helm** repository if needed :

```bash
helm repo update
```

Install the **Cert Manager** in its own namespace :

```bash
helm install cert-manager jetstack/cert-manager --namespace cert-manager --create-namespace --set installCRDs=true
```

Deploy the **Let's Encrypt** issuer :

```bash
kubectl apply -f kubernetes/letsencrypt-issuer.yaml
```

### Expose the service

Use the **Nginx** ingress controller.

Add the **Helm** repository of the **Nginx** ingress controller :

```bash
helm repo add ingress-nginx https://kubernetes.github.io/ingress-nginx
```

Update **Helm** repository if needed :

```bash
helm repo update
```

Install the **Nginx** ingress controller in its own namespace :

```bash
helm -n ingress-nginx install ingress-nginx ingress-nginx/ingress-nginx --create-namespace
```

Deploy the ingress :

```bash
kubectl apply -f kubernetes/soundbuzz-ingress.yaml
```

## Deploy monitoring

The first step is to create the namespace for the monitoring tools :

```bash
kubectl create ns monitoring
```

### The Prometheus Operator

Deploy the **Helm** charts wich will deploy and configure :
* A **Grafana**
* A **Prometheus** database
* An **AlertManager**
* Some Exporters to control the kubernetes cluster

```bash
helm install monitoring prometheus-community/kube-prometheus-stack -n monitoring -f monitoring/kube-prometheus-stack.values.yaml
```

### The web probe

Deploy a **BlackBox** exporter to check the web sites availabilities :

```bash
kubectl apply -f monitoring/blackbox-exporter.yaml
```

> The domains checked are configured into the values file dedicated to the **Prometheus** operator.

### The proxy MS Teams

Deploy the **MS Teams** proxy for prometheus :

> Don't forget to complete the webhook URL in the values file.

```bash
helm install prometheus-msteams prometheus-msteams/prometheus-msteams --namespace monitoring -f monitoring/prometheus-msteams.values.yaml
```

#### Test of the proxy

Expose the proxy **MS Teams** on the local host :

```bash
kubectl port-forward service/prometheus-msteams 2000:2000 -n monitoring
```

Send an alert on the channel :

```bash
curl -X POST -d @monitoring/prometheus-msteams-alert.json http://localhost:2000/soundbuzz-supervision
```

Check the **MS Teams** channel :

![Alert](docs/monit.png)

## Helm deployment

Deploy the **helm** charts :

```bash
helm install soundbuzz helm -n soundbuzz
```

if needed, deploy with a custom values file :

> Take the original values file like template

```bash
helm install soundbuzz helm -n soundbuzz -f helm/values-prod.yaml
```

Learn more about this **Helm** charts [here](https://gitlab.com/mln-nicolas-b/soundbuzz/-/blob/main/helm).

## Load tests

Use **Locust** to test the load on the instances.

Install **Locust** following the [documentation](https://docs.locust.io/en/stable/installation.html)

Launch **Locust** :

```bash
locust --users 1000 --spawn-rate 100 -H https://app.soundbuzz.me -f locust/locustfile.py
```

> Then connect to the web server http://127.0.0.1:8089 for a user friendly interface or add the option `--headless` to bypass it.

![locust](docs/locust.png)
